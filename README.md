# Unnamed Food Data Search
A "search engine" for finding data about various food products, powered by [Open Food Facts'](https://world.openfoodfacts.org/) API.

# [Live Demo](https://unnamed-food-data-search.netlify.app/)

## Screenshot
![img.png](img.png)